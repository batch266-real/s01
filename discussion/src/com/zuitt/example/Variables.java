package com.zuitt.example;

public class Variables {
    /*
    * Data Types
    *   - int for integers
    *   - double for float
    *   - char for single characters
    *   - boolean for boolean values
    * Non-Primitive Data Types
    *   - String
    *   - Arrays
    *   - Classes
    *   - Interface
    * */
    public static void main(String[] args) {
        int age = 18;
        char middle_initial = 'V';
        boolean isLegalAge = true;
        System.out.println("The user age is "+ age);
        System.out.println("The user middle initial is "+ middle_initial);
        System.out.println("Is the use of legal age? "+ isLegalAge);
    }
}
