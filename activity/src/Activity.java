import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        Scanner myInput = new Scanner(System.in);
        System.out.println("First Name:");
        String firstName = myInput.nextLine();
        System.out.println("Last Name:");
        String lastName = myInput.nextLine();
        System.out.println("First Subject Grade:");
        double firstSubject = new Double(myInput.nextLine());
        System.out.println("Second Subject Grade:");
        double secondSubject = new Double(myInput.nextLine());
        System.out.println("Third Subject Grade:");
        double thirdSubject = new Double(myInput.nextLine());
        System.out.println("Good day, "+ firstName +" "+ lastName);
        System.out.println("Your grade average is: "+ (firstSubject+secondSubject+thirdSubject)/3);
    }
}
